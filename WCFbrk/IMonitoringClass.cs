﻿using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Collections;
using WCFbrk.Monitoring;
using System;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.IdentityModel.Claims;
using System.IO;


namespace WCFbrk
{
    [Guid("FB202BF3-9782-40C7-90F3-C98CC5D52E19")]
    internal interface IMonitoringClass
    {
        [DispId(1)]
        //4. описываем методы которые можно будет вызывать из вне
        DataTable GetDataTable();
        void SetDataTable(DataTable dtt);
        string[,] GetStringArrayFromCurentDT(string[,] columnName, string condition = null);
        string[,] GetStringArrayFromDT(DataTable curDT = null, string condition = null);
        int GetDTintValue(int row, string column, DataTable curDT = null);
        string GetDTStringValue(int row, string column, DataTable curDT = null);
        int GetDTintValue(int row, string column);
        string GetcurDTStringValue(int row, string column);
        string OpenService(string username, string password,string configPath);
        bool CloseService();
        bool GetPeriodCurrent();
        DataTable GetPeriodList();
        bool GetRegisterList();
        DataTable GetRegisterListByPeriod(int PeriodID);
        bool GetRegisterByID(int RegistrID);
        bool GetMonitoringDataOnPeriod(int PeriodID);
        bool GetMonitoringDataOnRegisters(int[] RegisterID);
        bool ApproveOnRegister(int[] RegisterID);
        bool UploadReject(DataTable TableInsurerReject);
        bool СancelRejectOnRegister(int[] RegisterID);
        bool GetExpertiseList();
        bool ExpertiseSave(byte ExpertiseTypeID, string FileName, byte[] Stream);
        bool ExpertiseDelete(DateTime InputDate, int XMLTypeID);
        object[] GetReestrsByID(int RegisterID, string[] registerArray, string[] peopleArray, string[] caseArray, string[] serviceArray);
        string[] GetTitleReestr(object RegisterID, object[] registerArray);
        object[,] GetCaseByRegID(int RegisterID, object[] peopleArray, object[] caseArray, object[] serviceArray, string BackUpFile);
        string SaveDS(string BackUpFile, bool loadReg = false, bool loadCases = false, int idReg = 0);

        int GetPeriodByID(int RegisterID=0 );
        object[,] GetDataTableCOMArray(object[] tamples, DataTable tempDt);
       
    }

    [Guid("443D74FD-C3C9-4996-A4C3-BD7E8C473B87"), ClassInterface(ClassInterfaceType.None)]
    public class MonitoringClass : IMonitoringClass //основные функции работы с мониторингом
    {
        public DataTable dt;
        public DataSet ds;
        public MonitoringServiceClient client ;

        public DataTable GetDataTable()//
        {
            return dt;

        }
        public void SetDataTable(DataTable dtt)/// Установка текущей таблицы
        {
            dt = dtt.Copy();

        }

        public string[,] GetStringArrayFromCurentDT(string[,] columnName, string condition = null) ///Конвертирует текущий DataTable в многомерный массив String, 1 -WCF,2 1c строка заголовок
        {
            DataRow[] drCondition = null;

            if (condition != null)
            {
                drCondition = dt.Select(condition);
            }
            string[,] strArray = new string[dt.Rows.Count + 1, dt.Columns.Count];

            if (drCondition != null)
            {
                strArray = new string[drCondition.Length + 1, dt.Columns.Count];

                //for (int i = 0; i < columnName.Length/2; i++)
                //{
                //    strArray[0, i] = dt.Columns[i].ColumnName;

                //}
                for (int i = 0; i < drCondition.Length; i++)
                {
                    
                    for (int j = 0; j < columnName.Length/2; j++)
                    {
                        strArray[i, j] = drCondition[i][columnName[0,1].ToString()].ToString();

                    }

                }

            }
            else
            {


                //for (int i = 0; i < dt.Columns.Count; i++)
                //{
                //    strArray[0, i] = dt.Columns[i].ColumnName;

                //}
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int t = i + 1;
                     for (int j = 0; j < columnName.Length/2; j++)
                    {
                         strArray[t, j] = dt.Rows[i][columnName[0,1].ToString()].ToString();

                    }

                }
            }
            return strArray;
        }
        public string[,] GetStringArrayFromDT(DataTable curDT = null, string condition = null) ///Конвертирует текущий DataTable в многомерный массив String, 1 строка заголовок
        {
            DataRow[] drCondition = null;
            if (curDT == null)
            {
                curDT = dt.Copy();
            }
            if (condition != null)
            {
                drCondition = curDT.Select(condition);

            }
            string[,] strArray = new string[curDT.Rows.Count + 1, curDT.Columns.Count];

            if (drCondition != null)
            {
                strArray = new string[drCondition.Length + 1, curDT.Columns.Count];
                for (int i = 0; i < curDT.Columns.Count; i++)
                {
                    strArray[0, i] = curDT.Columns[i].ColumnName;
                }
                for (int i = 0; i < drCondition.Length; i++)
                {
                    int t = i + 1;
                    for (int j = 0; j < curDT.Columns.Count; j++)
                    {
                        strArray[t, j] = drCondition[i][j].ToString();
                    }
                }
            }
            else
            {
                for (int i = 0; i < curDT.Columns.Count; i++)
                {
                    strArray[0, i] = curDT.Columns[i].ColumnName;

                }
                for (int i = 0; i < curDT.Rows.Count; i++)
                {
                    int t = i + 1;
                    for (int j = 0; j < curDT.Columns.Count; j++)
                    {
                        strArray[t, j] = curDT.Rows[i][j].ToString();
                    }
                }
            }
            return strArray;
        }

        public int GetDTintValue(int row, string column, DataTable curDT = null)///Возвращение число данных из ячейки таблицы
        {
            if (curDT == null)
            {
                curDT = dt.Copy();
            }
            return (int)curDT.Rows[row][column];
        }
        public string GetDTStringValue(int row, string column, DataTable curDT = null)///Возвращение строковых данных из ячейки таблицы
        {
            if (curDT == null)
            {
                curDT = dt.Copy();
            }
            return (string)curDT.Rows[row][column];
        }
        public int GetDTintValue(int row, string column)///Возвращение число данных из ячейки таблицы
        {
            return (int)dt.Rows[row][column];
        }
        public string GetcurDTStringValue(int row, string column)///Возвращение строковых данных из ячейки таблицы
        {
            return (string)dt.Rows[row][column];
        }
      




        private static EndpointIdentity GetIdentity(string encodeValue)
        {
            //string encodeValue = "??" // here we should use the encoded certificate value generated by svcutil.exe.
            X509Certificate2Collection supportingCertificates = new X509Certificate2Collection();
            supportingCertificates.Import(Convert.FromBase64String(encodeValue));

            X509Certificate2 primaryCertificate = supportingCertificates[0];
            supportingCertificates.RemoveAt(0);
            return EndpointIdentity.CreateX509CertificateIdentity(primaryCertificate, supportingCertificates);
        }





        public string OpenService(string username, string password, string configPath)/// Открытие подключения к сервису мониторинга
        {
            if(client==null)
            {
              //  ServiceEndpoint serviceEndpoint = CreateDescription();
               // string conf = @"";
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = configPath;
                Configuration appConfig = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            
                

                ServiceModelSectionGroup smc = ServiceModelSectionGroup.GetSectionGroup(appConfig);

                
                WSHttpBinding binding = new WSHttpBinding();
                binding.OpenTimeout = smc.Bindings.WSHttpBinding.Bindings[0].OpenTimeout;
                binding.ReceiveTimeout = smc.Bindings.WSHttpBinding.Bindings[0].ReceiveTimeout;
                binding.SendTimeout = smc.Bindings.WSHttpBinding.Bindings[0].SendTimeout;
                binding.MaxReceivedMessageSize = smc.Bindings.WSHttpBinding.Bindings[0].MaxReceivedMessageSize;
                binding.ReaderQuotas.MaxArrayLength = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxArrayLength;
                binding.ReaderQuotas.MaxBytesPerRead = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxBytesPerRead ;
                binding.ReaderQuotas.MaxDepth = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxDepth ;
                binding.ReaderQuotas.MaxNameTableCharCount = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxNameTableCharCount;
                binding.ReaderQuotas.MaxStringContentLength = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxStringContentLength;
                binding.Name = smc.Bindings.WSHttpBinding.Bindings[0].Name;
                binding.Security.Message.ClientCredentialType = smc.Bindings.WSHttpBinding.Bindings[0].Security.Message.ClientCredentialType;
             

                EndpointAddress remoteAddress = new EndpointAddress(smc.Client.Endpoints[0].Address, GetIdentity(smc.Client.Endpoints[0].Identity.Certificate.EncodedValue));
                


                client = new MonitoringServiceClient(binding, remoteAddress);
                //client.c.Name = smc.Client.Endpoints[0].Name;
                //client. = smc.Client.Endpoints[0].Binding;
                //appConfig.AppSettings.

                
            }

            if (client.State == System.ServiceModel.CommunicationState.Opened)
            {
                try
                {
                    client.Close();
                }
                catch
                { }
            }
            
            try
            {
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;
                return "";
            }
            catch
            {
                return "";
            }

        }
        public bool CloseService() ///Закрытие подключения к сервису мониторинга
        {
            if (client.State == System.ServiceModel.CommunicationState.Closed)
            {
                return true;
            }
            try
            {
                client.Close();
                return true;
            }
            catch
            { return false; }
        }

        public string[,] GetRowDT(int RegisterID, string[,] rTitle)///возвращает заголовк счета   / массив входной  1 - данные  2 - поле1с 3 -поле WCF
        {
            if (GetRegisterByID(RegisterID))
            {
                for (int i = 0; i < rTitle.Length / 3; i++)
                {
                    rTitle[i, 0] = dt.Rows[0][rTitle[i, 2]].ToString();
                }
                return rTitle;
            }
            else
            {
                return new string[0, 0];
            }

        }

        public void ClearArray(string[,] arr)
        {
            for (int i = 0; i < arr.Length / 3; i++)
            {
                arr[i, 0] = "";
            }
        }


        #region ServiceMethod

        public bool GetPeriodCurrent() /// Функция запроса описания текущего открытого периода
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
            }
            dt = client.GetPeriodCurrent();
            if (dt != null && dt.Rows.Count > 0) return true;
            else return false;

        }

        public DataTable GetPeriodList() /// Функция запроса списка всех периодов
        {

           DataTable retdt = client.GetPeriodList();
           
            return retdt;
        }

        public bool GetRegisterList() /// Функция запроса списка всех реестров
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
            }
            dt = client.GetRegisterList();
            if (dt != null && dt.Rows.Count > 0) return true;
            else return false;
        }

        public DataTable GetRegisterListByPeriod(int PeriodID) /// Функция запроса списка реестров за указанный период
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
            }
            return  client.GetRegisterListByPeriod(Convert.ToByte(PeriodID));
  //          if (dt != null && dt.Rows.Count > 0) 
  //return true;
  //          else return false;
        }
        public bool GetRegisterByID(int RegistrID) /// Функция запроса реестра по ID реестра
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
            }
            dt = client.GetRegisterByID(RegistrID);
            if (dt != null && dt.Rows.Count > 0) return true;
            else return false;

        }
        public bool GetMonitoringDataOnPeriod(int PeriodID) ///Функция запрос данных мониторинга за выбранный период 
        {
             if (ds != null)
            {
                ds.Clear();
                ds.Dispose();
            }
            ds = client.GetMonitoringDataOnPeriod(Convert.ToByte(PeriodID));
            if (ds != null) return true;
            else return false;
        }
        public bool GetMonitoringDataOnRegisters(int[] RegisterID) /// Функция запрос данных мониторинга по перечисленным реестрам
        {
            if (ds != null)
            {
                ds.Clear();
                ds.Dispose();
            }
            ds = client.GetMonitoringDataOnRegisters(RegisterID);

            if (ds != null)
            {
                FurChangDS();
                return true;
            }
            else return false;
        }
        public void FurChangDS()
        {

           ds.Tables["Service"].Columns.Add("SUMV_USL", Type.GetType("System.Decimal"));
           for (int i =0;i<ds.Tables["Service"].Rows.Count; i++) //SUMV_USL
           {
               ds.Tables["Service"].Rows[i]["SUMV_USL"] = Math.Round(Convert.ToDecimal(ds.Tables["Service"].Rows[i]["SUM_USL"].ToString()) * Convert.ToDecimal(ds.Tables["Service"].Rows[i]["K_IND"].ToString()??"1") *
               Convert.ToDecimal(ds.Tables["Service"].Rows[i]["K_S"].ToString() ?? "1"), 2);
               ds.Tables["Service"].Rows[i]["ToothNumber"] = ds.Tables["Service"].Rows[i]["ToothNumber"] ?? 0;
             

           }
            
           
        }
        public bool ApproveOnRegister(int[] RegisterID) /// Функция одобрения списка реестров без отказов
        {
            return client.ApproveOnRegister(RegisterID);
        }
        public bool UploadReject(DataTable TableInsurerReject) /// Функция загрузки отказов
        {
            return client.UploadReject(TableInsurerReject);
        }
        public bool СancelRejectOnRegister(int[] RegisterID) /// Функция аннулирование результатов по реестрам
        {
            return client.СancelRejectOnRegister(RegisterID);
        }
        public bool GetExpertiseList() /// Функция запрос списка экспертиз
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
            }
            dt = client.GetExpertiseList();
            if (dt != null && dt.Rows.Count > 0) return true;
            else return false;
        }
        public bool ExpertiseSave(byte ExpertiseTypeID, string FileName, byte[] Stream) /// Функция добавление файла с экспертизой
        {
            return client.ExpertiseSave(ExpertiseTypeID, FileName, Stream);
        }
        public bool ExpertiseDelete(DateTime InputDate, int XMLTypeID) /// Функция удаление файла с экспертизой
        {
            return client.ExpertiseDelete(InputDate, Convert.ToByte(XMLTypeID));
        }


        #endregion MethodService

        #region GetReestrs


        public object[] GetReestrsByID(int RegisterID, string[] registerArray, string[] peopleArray, string[] caseArray, string[] serviceArray)
        {
            object[] retArray = new object[2];

            //вернутьЗаголовок
           

            //вернутьСЛучаи


            return new object[0];
        }

        public string[] GetTitleReestr(object RegisterID, object[] registerArray)
        {
            try
            {
                //string[] registerArray = (string[])registerA;
                GetRegisterByID((Int32)RegisterID);
                string[] title = new string[registerArray.Length];
                for (int i = 0; i < registerArray.Length; i++)
                {
                    title[i] = dt.Rows[0][registerArray[i].ToString()].ToString();//данные
                }
                return title;
            }
            catch (Exception ex )
            {
                string[] title = new string[3];
                title[0] = "ERROR";
                title[1] = ex.Message;
                title[2] = ex.StackTrace;
                return title;
            }
          
        }

        public object[,] GetCaseByRegID(int RegisterID, object[] peopleArray, object[] caseArray, object[] serviceArray, string BackUpFile) //3 элемента в массиве   0- персданные 1- 
        {
            try
            {
                //выборка рееестра
                int[] rID = new int[1];
                rID[0] = RegisterID;
                GetMonitoringDataOnRegisters(rID);

                int countCase = ds.Tables["Case"].Rows.Count;
                object[,] cases = new object[countCase, 5];

                int tekCase = 0;

                for (int i = 0; i < ds.Tables["People"].Rows.Count; i++)
                {
                    string[] peopleTemp = new string[ peopleArray.Length];
                    for (int j = 0; j < peopleArray.Length ; j++)
                    {
                        peopleTemp[j] = ds.Tables["People"].Rows[i][peopleArray[j].ToString()].ToString();

                    }
                    DataRow[] rCases = ds.Tables["Case"].Select("PersonID = " + ds.Tables["People"].Rows[i]["PersonID"].ToString() + "");
                    foreach (DataRow rcase in rCases)
                    {
                        cases[tekCase, 0] = peopleTemp;
                        string[] caseTemp = new string[caseArray.Length];
                        for (int iC = 0; iC < caseArray.Length; iC++)
                        {
                            caseTemp[iC] = rcase[caseArray[iC].ToString()].ToString();

                        }
                        cases[tekCase, 3] = rcase["TypeID"].ToString();
                        cases[tekCase, 4] = rcase["USL_OK"].ToString();

                        cases[tekCase, 1] = caseTemp;

                        DataRow[] rServices = ds.Tables["Service"].Select("CaseID = " + ds.Tables["Case"].Rows[i]["CaseID"].ToString() + " AND TypeID = " + ds.Tables["Case"].Rows[i]["TypeID"].ToString() + "");

                        string[,] serviceTemp = new string[rServices.Length, serviceArray.Length];
                        int rsit = 0;
                        foreach (DataRow rservice in rServices)
                        {
                            for (int iS = 0; iS < serviceArray.Length; iS++)
                            {
                                serviceTemp[rsit, iS] = rservice[serviceArray[iS].ToString()].ToString();

                            }
                            rsit++;
                        }

                        cases[tekCase, 2] = serviceTemp;
                        tekCase++;
                    }

                }


                SaveDS(BackUpFile, false, false);



                return cases;
            }
            catch (Exception ex)
            {
                object[,] title = new object[3,1];
                title[0,0] = "ERROR";
                title[1,0] = ex.Message;
                title[2,0] = ex.StackTrace;
                return title;

            }
        }

        public string SaveDS(string BackUpFile, bool loadReg = false, bool loadCases = false, int idReg = 0)
        {
            if (loadReg)
            {
                GetRegisterByID(idReg);

            }
            if (loadCases)
            {
                GetMonitoringDataOnRegisters(new int[] { idReg });

            }

            ds.Tables.Add(dt);
            string bckFile = BackUpFile + ds.Tables["Case"].Rows[0]["RegisterID"].ToString() + "_" + DateTime.Now.ToString("yyyy_MM_dd");
            StreamWriter sw = new StreamWriter(bckFile);
            ds.WriteXml(sw);
            sw.Close();
            return bckFile;

        }
        public int GetPeriodByID(int RegisterID=0)
        {
            if(RegisterID!=0)
            {
                int[] rID = new int[1];
                rID[0] = RegisterID;
                GetMonitoringDataOnRegisters(rID);
            }

            int period= int.Parse(dt.Rows[0]["PeriodID"].ToString());
            DataTable periodDT=  GetPeriodList();

            DataRow [] dr = periodDT.Select("PeriodID=" + period.ToString());
            if(dr.Length>0)
            {
              DateTime dtm =DateTime.Parse(dr[0]["ServiceEndDate"].ToString());


              int retInt = int.Parse(dtm.ToString("yyyyMM"));
              return retInt;
            }
            else
            {
                return 0;
            }
            //return 1;
        }

        #endregion GetReestrs

        public object[,] GetDataTableCOMArray ( object[] tamples, DataTable tempDt=null)
        {
            if (tempDt==null)
            {
                tempDt = dt;

            }
            try
            {
                object[,] retArray = new object[tempDt.Rows.Count, tamples.Length];
                for (int i = 0; i < tempDt.Rows.Count; i++)
                {
                    for (int j = 0; j < tamples.Length; j++)
                    {
                        retArray[i, j] = tempDt.Rows[i][tamples[j].ToString()].ToString();
                    }
                }
                return retArray;
            }
            catch (Exception ex)
            {
                object[,] retArray = new object[3, 1];
                retArray[0, 0] = "ERROR";
                retArray[1, 0] = ex.Message;
                retArray[2, 0] = ex.StackTrace;

                return retArray;

            }
        }


        //public void GetComSafeArray()
        //{
        //    ComSafeArray



        //}


    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Collections;
//using V83;
using WCFbrk.Monitoring;


namespace WCFbrk
{
   
    [Guid("1B8AD27D-1BD6-49EB-B669-AA5F15ECAD46")]
    internal interface IMyClass
    {
        [DispId(1)]
        //4. описываем методы которые можно будет вызывать из вне
        string PrintIn1C(string mymessage);
        void OpenCD(bool MyParam);
        string GetAllSystemProcess();
        string SpeakVoice(string SpeakText);
        DataTable GetDT();
        object[] GetDTarray(DataTable dt);
        string[,] ret(string dddd);
        
    }
    [Guid("12E5D67D-010A-4793-B351-14283D0E9388"), InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IMyEvents
    {
    }
    [Guid("69EE0677-884A-4eeb-A3BD-D407844C0C70"), ClassInterface(ClassInterfaceType.None), ComSourceInterfaces(typeof(IMyEvents))]
    public class MyClass : IMyClass //название нашего класса MyClass
    {
        
        

      //  MonitoringServiceClient client = new MonitoringServiceClient();
        string StringProcess; //строковая переменная для функции ниже
        [DllImport("winmm.dll")] //подключаем dll для процедуры ниже
        static extern Int32 mciSendString(String command, StringBuilder buffer, Int32 bufferSize, IntPtr hwndCallback);
        //простенькая функция которая вернет строку в 1С которую мы также пошлем из 1С, можете написать ей Hello World к примеру
        public string PrintIn1C(string mymessage)
        {
            return mymessage;
        }


        public string[,] ret (string dddd)
        {
            string[,] rtt = new string[3, 2];
            return rtt;
        }
        //процедура для открытия/закрытия CD-ROM'а из 1С ;-)
        public void OpenCD(bool MyParam)
        {
            if (MyParam)
            {
                mciSendString("set CDAudio door open", null, 0, IntPtr.Zero);
            }
            else
                mciSendString("set CDAudio door closed", null, 0, IntPtr.Zero);
        }
        //получаем список процессов и передаем 1С
        public string GetAllSystemProcess()
        {
            foreach (System.Diagnostics.Process winProc in System.Diagnostics.Process.GetProcesses())
            {
                StringProcess = StringProcess + "\r\n Process " + winProc.Id + ": " + winProc.ProcessName;
            }
            return StringProcess;
        }

        public DataTable GetDT()
        {

            DataTable dt = new DataTable();

            dt.Columns.Add("int", Type.GetType("System.Int32"));
            dt.Columns.Add("string", Type.GetType("System.String"));
            dt.Columns.Add("boolean", Type.GetType("System.Boolean"));


            dt.Rows.Add(11, "111111111111", false);
            dt.Rows.Add(22, "222222222222", true);
        

            return dt;
        }


        public object[] GetDTarray(DataTable dt )
        {
            
            
            object[] obj = new object[dt.Rows.Count];
            

            for (int i = 0; i < dt.Rows.Count;i++ )
            {
                string[,] strtt = new string[2, dt.Columns.Count];
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strtt[0, j] = dt.Columns[j].ColumnName.ToString();
                    strtt[1, j] = dt.Rows[i][j].ToString();

                }
                obj[i]=(object)strtt;
            }


            return obj;
        }
        //кое-что поинтересней процедура для вызова "говорящего компьютера" передаем ей текст, а она нам голосом!:)
        public string SpeakVoice(string SpeakText)
        {
            //SpVoice voice = new SpVoice();
            //voice.Speak(SpeakText, SpeechVoiceSpeakFlags.SVSFDefault);
            return SpeakText+"хрень";
        }
    }

}

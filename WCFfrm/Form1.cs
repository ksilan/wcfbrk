﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Text;
using System.Windows.Forms;
//using WCFbrk;
using WCFfrm.Monitoring;
using WCFfrm.NSIInsurance;

namespace WCFfrm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        { 
           // DataTable result;
            MonitoringServiceClient client = new MonitoringServiceClient();
           
            client.ClientCredentials.UserName.UserName = "SMO2";
            client.ClientCredentials.UserName.Password = "No0YQYN3";
            //byte x = 19;
            //result = client.GetRegisterByID(9351);
            //result = client.GetRegisterByID(9353);
            //dataGridView1.DataSource = result;
            int[] tt = new int[1];
            tt[0] = 9353;
            DataSet ds = client.GetMonitoringDataOnRegisters(tt);
            if (ds != null)
            {

                //dataGridView1.DataSource = result;
                BindingSource people = new BindingSource();
                BindingSource bsPersonID = new BindingSource();
                BindingSource bsCaseID = new BindingSource();
                BindingSource bsTypeID = new BindingSource();


                people.DataSource = ds;
                people.DataMember = "People";
                bsPersonID.DataSource = people;
                bsPersonID.DataMember = "People_Case_PersonID";
                bsCaseID.DataSource = bsPersonID;
                bsCaseID.DataMember = "Case_Service_CaseID_TypeID";

                dataGridView1.DataSource = people;
                //    dataGridView1.DataBindings.Add("PersonID", ds, "People",false);

                dataGridView2.DataSource = bsPersonID;
                // dataGridView2.DataMember = "Case";
                //dataGridView2.DataBindings

                dataGridView3.DataSource = bsCaseID;
                // dataGridView3.DataMember = "Service";
            }
            //// Используйте переменную "client", чтобы вызвать операции из службы.

            // Всегда закройте клиент.
            client.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {


            //DataTable result;
            //NSIInsuranceServiceClient client = new NSIInsuranceServiceClient();
             MonitoringServiceClient client = new MonitoringServiceClient();
           
            client.ClientCredentials.UserName.UserName = "SMO2";
            client.ClientCredentials.UserName.Password = "No0YQYN3";
            //byte x = 19;
            client.Open();
           // dataGridView1.DataSource = result;
            //int[] tt = new int[1];
            //tt[0] = 9353;
            DataTable dt = client.GetRegisterListByPeriod(16);
           if (dt != null)
            {

                dataGridView2.DataSource = dt;
            }
            //// Используйте переменную "client", чтобы вызвать операции из службы.

            // Всегда закройте клиент.
           client.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            ////"SMO2";
            ////"No0YQYN3";
            //MonitoringClass mC = new MonitoringClass();
            //mC.OpenService("SMO2", "No0YQYN3");
            //mC.CloseService();
        }

        private void button4_Click(object sender, EventArgs e)
        {

       
           // // ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
           // // fileMap.ExeConfigFilename = configPath;
           // // Configuration appConfig = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            
           
           // //ServiceModelSectionGroup smc = ServiceModelSectionGroup.GetSectionGroup(appConfig);



          


           // BasicHttpBinding binding = new BasicHttpBinding();
           // binding.OpenTimeout = smc.Bindings.WSHttpBinding.Bindings[0].OpenTimeout;
           // binding.ReceiveTimeout = smc.Bindings.WSHttpBinding.Bindings[0].ReceiveTimeout;
           // binding.SendTimeout = smc.Bindings.WSHttpBinding.Bindings[0].SendTimeout;
           // binding.MaxReceivedMessageSize = smc.Bindings.WSHttpBinding.Bindings[0].MaxReceivedMessageSize;
           // binding.ReaderQuotas.MaxArrayLength = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxArrayLength;
           // binding.ReaderQuotas.MaxBytesPerRead = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxBytesPerRead;
           // binding.ReaderQuotas.MaxDepth = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxDepth;
           // binding.ReaderQuotas.MaxNameTableCharCount = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxNameTableCharCount;
           // binding.ReaderQuotas.MaxStringContentLength = smc.Bindings.WSHttpBinding.Bindings[0].ReaderQuotas.MaxStringContentLength;
           // EndpointAddress remoteAddress = new EndpointAddress(smc.Client.Endpoints[0].Address, GetIdentity(smc.Client.Endpoints[0].Identity.Certificate.EncodedValue));

           //MonitoringServiceClient client = new MonitoringServiceClient(binding, remoteAddress);
        }

        private static EndpointIdentity GetIdentity(string encodeValue)
        {
            //string encodeValue = "??" // here we should use the encoded certificate value generated by svcutil.exe.
            X509Certificate2Collection supportingCertificates = new X509Certificate2Collection();
            supportingCertificates.Import(Convert.FromBase64String(encodeValue));

            X509Certificate2 primaryCertificate = supportingCertificates[0];
            supportingCertificates.RemoveAt(0);
            return EndpointIdentity.CreateX509CertificateIdentity(primaryCertificate, supportingCertificates);
        }


    }
}
